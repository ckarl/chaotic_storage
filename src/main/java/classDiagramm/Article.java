package classDiagramm;

public class Article {

    private String name;
    private String description;
    private String frequency;
    private int id;
    private boolean isNew;
    private boolean isAdditionalDelivery;
    // TODO 007: 0..* Assignment 1..* Article: Assignment holds article ids

    // TODO Fix class diagramm: Java standard says isHighFrequency instead of IsHighFrequency
    public boolean isHighFrequency() {
        return false;
    }

    public boolean isNew() {
        return isNew;
    }

    public boolean isAdditionalDelivery() {
        return isAdditionalDelivery;
    }

    public void setToHighFrequency() {
        this.frequency = "high";
    }

    public void setToNormalArticle() {

    }

    public void isNew(boolean isNew) {
        this.isNew = isNew;
    }

    public void isAdditionalDelivery(boolean isAdditionalDelivery) {
        this.isAdditionalDelivery = isAdditionalDelivery;
    }
}
