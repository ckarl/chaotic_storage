package classDiagramm.notYetIncluded;

import classDiagramm.*;

public class Worker {

    public void unpacks(Article article) {

    }

    public void registers(Article article) {

    }

    public void navigatesCarToArea(Car car, Area_ area) {
    }

    public void detectArticleAsNew(Article article) {
        article.isNew(true);
    }

    public void detectArticleAsAdditionalDelivery(Article article) {
        article.isAdditionalDelivery(true);
    }

    public ArticleList pickUpArticleListFrom(DeliveryStation deliveryStation) {
        return new ArticleList();
    }

    public void submitListTo(List_ list, DeliveryStation deliveryStation) {

    }
}
