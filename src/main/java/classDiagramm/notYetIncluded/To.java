package classDiagramm.notYetIncluded;

import classDiagramm.*;

public class To {
    public static ToPallet pallet(Pallet pallet) {
        return new ToPallet(pallet);
    }

    public static ToArticle article(Article article) {
        return new ToArticle(article);
    }

    public static ToCar car(Car car) {
        return new ToCar(car);
    }
}
