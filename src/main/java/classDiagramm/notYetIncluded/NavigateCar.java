package classDiagramm.notYetIncluded;

import classDiagramm.*;

public class NavigateCar {
    private final Car car;
    private Area_ area;
    private Worker staff;

    public NavigateCar(Car car) {
        this.car = car;
    }

    public NavigateCar toArea(Area_ area) {
        this.area = area;
        return this;
    }

    public NavigateCar byStaff(Worker staff) {
        this.staff = staff;
        return this;
    }

    public void now() {

    }
}
