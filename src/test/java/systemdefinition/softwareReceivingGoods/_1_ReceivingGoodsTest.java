package systemdefinition.softwareReceivingGoods;

import classDiagramm.*;
import classDiagramm.notYetIncluded.*;
import org.junit.*;

import static org.junit.Assert.*;

// TODO Systembeschreibung Software Lagerhaltung Prozess Warenannahme Prozess: Warenannahme
// TODO 001: Check description with activity diagram
public class _1_ReceivingGoodsTest {

    @Test
    public void phase_1() {
        // TODO Die Waren kommen im Versandhaus an und werden direkt von den Mitarbeiter ausgepackt.
        MailOrderFirm firm = new MailOrderFirm();
        Article article = new Article();
        firm.alertsArrivalOf(article);
        Worker worker = new Worker();
        worker.unpacks(article);

        // TODO Danach erfassen die Mitarbeiter die Waren in der Software.
        worker.registers(article);

        // TODO Denn so können sie dann erst feststellen, ob es
        //  sich um eine Nachlieferung bereits vorhandener Ware
        //  oder um eine Neuware handelt.
        worker.detectArticleAsNew(article);
        assertTrue(article.isNew());
        worker.detectArticleAsAdditionalDelivery(article);
        assertTrue(article.isAdditionalDelivery());
    }

    @Test
    public void phase_2() {
        // TODO Die Software prüft dann die Neuware und stellt fest, ob es eine normale Ware ist
        //      oder eine hochfrequenzierte Ware ist.
        Article article = new Article();
        boolean isHighFrequency = article.isHighFrequency();
        assertFalse(isHighFrequency);

        // TODO Die hochfrequenzierte Waren werden dann auf Paletten gelagert.
        Article highFrequencyArticle = new Article();
        highFrequencyArticle.setToHighFrequency();
        Pallet pallet = new Pallet();
        boolean added = To.pallet(pallet).add(highFrequencyArticle);
        assertTrue(added);

        // TODO Die normale Waren bekommen eher einen Aufkleber mit Barcode.
        Article normalArticle = new Article();
        normalArticle.setToNormalArticle();
        To.article(normalArticle).addBarCodeLabel();

        // TODO Die Die Waren werden dann auf den Wagen aufgeladen bis der Wagen voll wird.
        Car car = new Car();
        To.car(car).untilFullAdd(article);

        // TODO Das System erstellt auch zugleich eine Packliste mit den folgenden Informationen
        //        - Der Bereich wo der Wagen hinkommt
        //        - Der Hinweis welcher Artikel in welches Regal und
        //        - auf welchen Lagerplatz kommt.
        Assignment_ assignment = For.car(car).createAssignment();
        Area_ area = assignment.getArea();
        Rack rack = assignment.getRackOfFirstArticle();
        StorageArea storageArea = rack.getStorageArea();
    }

    @Test
    public void phase_3() {

        // TODO: Das System überprüft dann, ob der Wagen voll wird.
        Car car = new Car();
        boolean isFull = Check.car(car).isFull();

        // TODO: Ist der Wagen voll, gibt das System automatisch die Packliste aus.
        Create.assignmentOf(car);

        // TODO: Anschließend wird der Wagen in der Zielzone gebracht.
        Worker staff = new Worker();
        Area_ area = new Area_();
        Navigate.car(car).toArea(area).byStaff(staff).now();
    }
}
