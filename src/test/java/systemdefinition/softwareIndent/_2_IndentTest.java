package systemdefinition.softwareIndent;

import org.junit.*;

// TODO Systembeschreibung Software Lagerhaltung Prozess Warenbestellungs

public class _2_IndentTest {

    @Test
    public void phase_1() {
        /*
        Phase1:
        Nachdem die Bestellungen über den Onlineshop eingegangen sind, werden sie von der Software geprüft.
        Die Software ermittelt wo sich die Artikeln mit möglichst wenig Laufweg befinden
            und gibt den Auftrag an die Abgabestation weiter.
        Sollte bei der Überprüfung von der Software festgestellt, dass einen Auftrag nicht nur auf einer Etage
            abgearbeitet werden könnte, so wird der Auftrag in zwei oder mehrere Teilaufträge aufgeteilt.
         */
    }

    @Test
    public void phase_2() {
        /*
        Phase2:
        Der Mitarbeiter holt sich dann an die Abgabestation die von der Software erstellten Liste mit diverse Infos ab.
        Der Mitarbeiter erfüllt den Auftrag bzw. stellt die bestellten Artikeln bereit und gibt die Artikeln dann
            beim Versandbereich.
        Der Mitarbeiter holt sich anschließend eine neue Liste ab.
         */
    }
}
