package activities;

import classDiagramm.*;
import classDiagramm.notYetIncluded.*;
import com.sun.jmx.remote.util.*;
import org.junit.*;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WorkerUnpackProductsTest {

    @Test
    public void worker_unpackProduct_registerProductsInSystem_detectAdditionalDelivery() {
        // TODO 002: rename product to article in activity diagram. equal class diagram naming.
        Worker worker = new Worker();
        Article article = new Article();
        worker.unpacks(article);
        worker.registers(article); // TODO 003: "...InSystem" seems obsolet in activity diagram
        worker.detectArticleAsAdditionalDelivery(article);
        assertTrue(article.isAdditionalDelivery());
    }

    @Test
    public void worker_unpackProduct_registerProductsInSystem_detectNew() {
        // TODO 002: rename product to article in activity diagram. equal class diagram naming.
        Worker worker = new Worker();
        Article article = new Article();
        worker.unpacks(article);
        worker.registers(article); // TODO 003: "...InSystem" seems obsolete in activity diagram
        worker.detectArticleAsNew(article);
        assertTrue(article.isNew());
    }

    @Test
    public void worker_pickUpDescriptionList_submitListToShipping() {
        // TODO FIXME rename descriptionlist in activity diagram to articlelist as in class diagram
        Worker worker = new Worker();
        DeliveryStation deliveryStation = new DeliveryStation();
        ArticleList articleList = worker.pickUpArticleListFrom(deliveryStation);
        assertNotNull(articleList); // TODO: article list holds things. needs to be implemented
        List_ list = new List_();
        worker.submitListTo(list, deliveryStation ); // TODO: More details on how worker gets articles for order.
        // TODO Rename Shipping to DeliveryStation.
    }

    @Test
    public void system_checkNewProductType_detectNormalProduct_assignStickerWithBarCode_loadUpProductOnTheCar_storeProducts() {
        // TODO FIXME "loadUpProductOnTheCar" is not done by the system, but by a worker
        // TODO FIXME "storeProducts" is not done by the system, but by a worker
        Article normalArticle = new Article();
        normalArticle.setToNormalArticle();
        To.article(normalArticle).addBarCodeLabel(); // TODO create System class to handle actions?

        Car car = new Car();
        To.car(car).untilFullAdd(normalArticle);
        Assignment_ assignment = For.car(car).createAssignment();
        Area_ area = assignment.getArea();
        Rack rack = assignment.getRackOfFirstArticle();
        StorageArea storageArea = rack.getStorageArea();

        Worker worker = new Worker();
        worker.navigatesCarToArea(car, area);
     }

    @Test
    public void system_checkNewProductType_detectNormalProduct_assignStickerWithBarCode_createPackingList() {
        // TODO FIXME "storeProducts" is not done by the system, but by a worker
        Article normalArticle = new Article();
        normalArticle.setToNormalArticle();
        To.article(normalArticle).addBarCodeLabel(); // TODO create System class to handle actions?

        Car car = new Car();
        To.car(car).untilFullAdd(normalArticle);
        Assignment_ assignment = For.car(car).createAssignment();
        Area_ area = assignment.getArea();
        Rack rack = assignment.getRackOfFirstArticle();
        StorageArea storageArea = rack.getStorageArea();

        Worker worker = new Worker();
        worker.navigatesCarToArea(car, area);
    }

    @Test
    public void system_checkNewProductType_detectHighFrequencyProduct() {
        Article highFrequencyArticle = new Article();
        highFrequencyArticle.setToHighFrequency();
        assertTrue(highFrequencyArticle.isHighFrequency());
        Pallet pallet = new Pallet();
        boolean added = To.pallet(pallet).add(highFrequencyArticle);
        assertTrue(added);
    }

    @Test
    public void system_checkCompletenessOfTheCar() {
        Car car = new Car();
        Check.car(car).isComplete();
    }

    @Test
    public void system_checkOrder_splitItIntoPartialOrders() {
        Order order = new Order();
        boolean isToBeSplitIntoPartialOrders = Check.order(order).isToBeSplitIntoPartialOrders();
        Order[] partialOrders = Split.order(order).intoPartialOrders();
    }

    @Test
    public void system_checkOrder_passOrderToDeliverStation() {
        Order order = new Order();
        DeliveryStation deliveryStation = new DeliveryStation();
        Check.order(order).passOrderToDeliveryStation(deliveryStation);

    }
}